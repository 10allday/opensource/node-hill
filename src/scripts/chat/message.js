const PacketBuilder = require("../../net/PacketBuilder").default

const filterModule = require("../../util/filter/filterModule").default

const Game = require("../../class/Game").default

const generateTitle = require("../../util/chat/generateTitle").default

const formatHex = require("../../util/color/formatHex").default

const COLOR_TAG_REGEX = new RegExp("(<color:[0-9]{6}>)|(\\c[0-9]{1})", "g")

const MAX_CHAT_LENGTH = 84

const MAX_COLOR_TAGS = 5

const rateLimit = new Set()

function getColorTagCount(input) {
    return input.match(COLOR_TAG_REGEX)?.length || 0
}

function clientMessageAll(p, message, titleGeneration) {
    // Player is rate-limited
    if (rateLimit.has(p.userId))
        return p.message("You're chatting too fast!")

    // Add player to rate-limit
    rateLimit.add(p.userId)
    setTimeout(() => rateLimit.delete(p.userId), Game.chatSettings.rateLimit)

    // Player is muted
    if (p.muted)
        return p.message("You are muted.")
    // Player used a swear
    if (filterModule.isSwear(message))
        return p.message("Don't swear! Your message has not been sent.")

    const tagCount = getColorTagCount(message)

    const messageLengthWithoutTags = message.length - (tagCount * 14)

    // No message
    if (!messageLengthWithoutTags)
        return p.message("Chat message cannot be empty.")

    // Player is using too many color tags
    if (tagCount > MAX_COLOR_TAGS)
        return p.message("You can only use 5 or less color tags!")

    // Message is too long
    if (messageLengthWithoutTags > MAX_CHAT_LENGTH)
        return p.message("Message is too long.")

    console.log(`${p.username}: ${message}`)

    Game.emit("chatted", p, message)

    p.emit("chatted", message)

    let fullMessage = message

    if (titleGeneration) fullMessage = generateTitle(p, message)

    return new PacketBuilder("Chat")
        .write("string", fullMessage)
        .broadcastExcept(p.getBlockedPlayers())
}

function messageAll(message) {
    message = formatHex(message)

    return new PacketBuilder("Chat")
        .write("string", message)
        .broadcast()
}

function messageClient(socket, message) {
    message = formatHex(message)

    return new PacketBuilder("Chat")
        .write("string", message)
        .send(socket)
}

module.exports = { messageAll, messageClient, clientMessageAll }