/*eslint no-undef: "off"*/
async function load(options) {
    require("nh-admin")

    if (options === "object")
        Object.assign(Game.cheatsAdmin, options)

    if (Game.local)
        Game.on("playerJoin", (p) => Game.cheatsAdmin.owners.push(p.userId))
    else
        if (Game.gameId)
            try {
                const res = await Game.getSetData(Game.gameId)
                if (res.error) throw res.error.message
                const creator = res.data.creator

                if (!Game.cheatsAdmin.owners.includes(creator.id)) {
                    Game.cheatsAdmin.owners.push(creator.id)
                    console.log(`[cheatsAdmin] Set ${creator.username} as admin.`)
                }
            } catch (err) {
                console.warn("[cheatsAdmin] Failed to add set owner as admin.")
            }

    return Game.cheatsAdmin
}

initCheatsAdmin = load